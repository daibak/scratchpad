# %% import

import asyncio
import random
import time
from dataclasses import asdict, dataclass
from typing import Type
from urllib.parse import urlparse

import httpx
import pandas as pd

from scratchpad.asyncio_executor import batch_execute

# %% worker


@dataclass(frozen=True, kw_only=True)
class SearchDetatils:
    children: int
    dep_month: int
    dep_weekday: int
    flight_cost: float
    hotel_n_nights: int
    infants: int
    lead_time: int
    pax: int


@dataclass(frozen=True, kw_only=True)
class TripDetails:
    hotel_cost: float
    hotel_stars: float
    ocre_baggage: float
    ocre_service_costs: float
    ocre_tep: float
    ocre_transfer: float


@dataclass(frozen=True, kw_only=True)
class Input:
    search: SearchDetatils
    trips: list[TripDetails]


@dataclass(frozen=True, kw_only=True)
class Output:
    markup: str
    response_time: float


@dataclass(frozen=True, kw_only=True)
class CommonWorkerParams[O]:
    api_endpoint: str
    method: str
    client: httpx.AsyncClient
    semaphore: asyncio.Semaphore
    output_class: Type[O]


async def worker[I, O](params: CommonWorkerParams[O], input: I) -> O:
    async with params.semaphore:
        time_start = time.monotonic()
        resp = await params.client.request(
            params.method, params.api_endpoint, json=asdict(input)
        )
        time_end = time.monotonic()

    resp.raise_for_status()
    out = params.output_class(**resp.json(), response_time=time_end - time_start)

    return out


# %% main


async def launch_test(
    url: str, method: str, inputs: list[Input], concurrency: int = 10
) -> dict[int, tuple[Input, Output]]:
    parsed_url = urlparse(url)

    async with httpx.AsyncClient(
        base_url=parsed_url._replace(path="").geturl()
    ) as client:
        params = CommonWorkerParams(
            method=method,
            api_endpoint=parsed_url.path,
            client=client,
            output_class=Output,
            semaphore=asyncio.Semaphore(concurrency),
        )
        out = await batch_execute(worker, params, inputs)

    return out


# %% main


def generate_random_input(min_trips: int, max_trips: int) -> Input:
    search = SearchDetatils(
        children=random.randint(0, 5),
        dep_month=random.randint(1, 12),
        dep_weekday=random.randint(0, 6),
        flight_cost=random.uniform(100.0, 1000.0),
        hotel_n_nights=random.randint(1, 14),
        infants=random.randint(0, 2),
        lead_time=random.randint(1, 365),
        pax=random.randint(1, 10),
    )
    trips = [
        TripDetails(
            hotel_cost=random.uniform(50.0, 500.0),
            hotel_stars=random.uniform(1.0, 5.0),
            ocre_baggage=random.uniform(0.0, 50.0),
            ocre_service_costs=random.uniform(0.0, 100.0),
            ocre_tep=random.uniform(0.0, 20.0),
            ocre_transfer=random.uniform(0.0, 30.0),
        )
        for _ in range(random.randint(min_trips, max_trips))
    ]
    return Input(search=search, trips=trips)


def main(args):
    inputs = [
        generate_random_input(args.min_trips, args.max_trips)
        for _ in range(args.num_inputs)
    ]

    start_time = time.monotonic()
    with asyncio.Runner() as runner:
        out = runner.run(launch_test(args.url, args.method, inputs, args.concurrency))
    end_time = time.monotonic()

    total_time = end_time - start_time
    throughput = args.num_inputs / total_time

    # Convert the output dictionary to a DataFrame and save it as a CSV file
    df = pd.json_normalize(
        [dict(index=k, input=asdict(i), **asdict(o)) for k, (i, o) in out.items()],
        sep="_",
    )
    df["n_trips"] = df["input_trips"].apply(len)

    print(f"Total time: {total_time:.2f} seconds")
    print(
        f"Throughput: {throughput:.2f} requests/second at concurrency level {args.concurrency}"
    )
    print(f"Mean Latency: {df['response_time'].mean():.2f} sec")
    print(f"Q90 Latency: {df['response_time'].quantile(0.9):.2f} sec")
    print(
        f"Mean throughput per trips: {df['n_trips'].div(df['response_time']).mean():.2f} trips/second at"
    )
    print(
        f"Total throughput per trips: {df['n_trips'].sum() / df['response_time'].sum():.2f} trips/second at"
    )

    df.to_csv(args.output_path, index=False)


# %% script run

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description="Stress test an API with asyncio.")
    parser.add_argument("url", type=str, help="The API endpoint URL.")
    parser.add_argument(
        "num_inputs", type=int, help="Number of random inputs to generate."
    )
    parser.add_argument(
        "--method",
        type=str,
        choices=["get", "post"],
        default="post",
        help="HTTP method to use.",
    )
    parser.add_argument(
        "-c",
        "--concurrency",
        type=int,
        default=10,
        help="Number of concurrent requests.",
    )
    parser.add_argument(
        "--min_trips", type=int, default=1, help="Minimum number of trips per input."
    )
    parser.add_argument(
        "--max_trips", type=int, default=5, help="Maximum number of trips per input."
    )
    parser.add_argument(
        "--output_path",
        type=str,
        default="output.csv",
        help="Path to save the output CSV file.",
    )
    args = parser.parse_args()
    main(args)
