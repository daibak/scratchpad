# %% import

import asyncio
from dataclasses import asdict, dataclass
from typing import Type
from urllib.parse import urlparse

import httpx

from scratchpad.asyncio_executor import batch_execute

# %% worker


@dataclass(frozen=True, kw_only=True)
class Input:
    text: str


@dataclass(frozen=True, kw_only=True)
class APIEntry:
    API: str
    Auth: str
    Category: str
    Cors: str
    Description: str
    HTTPS: bool
    Link: str


@dataclass(frozen=True, kw_only=True)
class Output:
    count: int
    entries: list[APIEntry]


@dataclass(frozen=True, kw_only=True)
class CommonWorkerParams[O]:
    api_endpoint: str
    method: str
    client: httpx.AsyncClient
    semaphore: asyncio.Semaphore
    output_class: Type[O]


async def worker[I, O](params: CommonWorkerParams[O], input: I) -> O:
    async with params.semaphore:
        resp = await params.client.request(
            params.method, params.api_endpoint, json=asdict(input)
        )

    resp.raise_for_status()
    out = params.output_class(**resp.json())

    return out


# %% main


async def main():
    url = "https://api.publicapis.org/entries"
    parsed_url = urlparse(url)
    inputs = [Input(text=f"test_{i}") for i in range(10)]

    async with httpx.AsyncClient(
        base_url=parsed_url._replace(path="").geturl()
    ) as client:
        params = CommonWorkerParams(
            method="get",
            api_endpoint=parsed_url.path,
            client=client,
            output_class=Output,
            semaphore=asyncio.Semaphore(10),
        )
        out = await batch_execute(worker, params, inputs)

    print(len(out))
    return out


# %% script run

if __name__ == "__main__":
    with asyncio.Runner() as runner:
        runner.run(main())
