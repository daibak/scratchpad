# %% import

import asyncio
from dataclasses import asdict, dataclass
from typing import Type
from urllib.parse import urlparse

import httpx

from scratchpad.asyncio_executor import batch_execute

# %% worker


@dataclass(frozen=True, kw_only=True)
class Input:
    text: str


@dataclass(frozen=True, kw_only=True)
class APIEntry:
    API: str
    Auth: str
    Category: str
    Cors: str
    Description: str
    HTTPS: bool
    Link: str


@dataclass(frozen=True, kw_only=True)
class Output:
    count: int
    entries: list[APIEntry]


@dataclass(kw_only=True)
class Worker[I, O]:
    api_endpoint: str
    method: str
    client: httpx.AsyncClient
    semaphore: asyncio.Semaphore
    output_class: Type[O]

    async def handler(self, input: I) -> O:
        async with self.semaphore:
            resp = await self.client.request(
                self.method, self.api_endpoint, json=asdict(input)
            )

        resp.raise_for_status()
        out = self.output_class(**resp.json())

        return out


# %% main


async def main():
    url = "https://api.publicapis.org/entries"
    parsed_url = urlparse(url)
    inputs = [Input(text=f"test_{i}") for i in range(10)]

    async with httpx.AsyncClient(
        base_url=parsed_url._replace(path="").geturl()
    ) as client:
        worker = Worker[Input, Output](
            method="get",
            api_endpoint=parsed_url.path,
            client=client,
            output_class=Output,
            semaphore=asyncio.Semaphore(10),
        )
        out = await batch_execute(Worker.handler, worker, inputs)

    print(len(out))
    return out


# %% script run

if __name__ == "__main__":
    with asyncio.Runner() as runner:
        runner.run(main())
