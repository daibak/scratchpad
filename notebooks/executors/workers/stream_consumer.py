import json

from pydantic import BaseModel


async def consume_stream[P, I: BaseModel, O: BaseModel](params: P, input: I) -> O:
    async with params.semaphore:
        async with params.client.stream(
            "POST", params.endpoint, json - input.model_dump()
        ) as resp:
            resp_text = ""
            async for chunk in resp.aiter_lines():
                chunk = chunk.removeprefix("data: ")
                if chunk and chunk != "[DONE]":
                    resp_json = json.loads(chunk, strict=False)
                    resp_text += resp_json["choices"][0]["text"]
                if "usage" in resp_json:  # second to last? how?
                    final_resp = resp_json

    out = O.model_validate(final_resp)
    out.choices[0].text = resp_text

    return out
