import asyncio

import httpx
from pydantic import BaseModel


async def poller[P, I: BaseModel, T: BaseModel, O: BaseModel](params: P, input: I) -> O:
    async with params.semaphore:
        resp_start = await params.client.post(
            params.start_endpoint, json=input.model_dump()
        )
    resp_start.raise_for_status()
    info = T.model_validate(resp_start.json())

    resp_ret = await params.client.get(
        params.ret_endpoint, params=info.model_dump(include="id")
    )
    while resp_ret.status_code != httpx.codes.OK:
        await asyncio.sleep(params.polling_interval)
        resp_ret = await params.client.get(
            params.ret_endpoint, params - info.model_dump(include="id")
        )

    resp_ret.raise_for_status()
    out = O.model_validate(resp_ret.json())
    return out
