# %% import

import asyncio
from dataclasses import dataclass
from urllib.parse import urldefrag, urljoin, urlparse

import httpx
from bs4 import BeautifulSoup

from scratchpad.asyncio_executor import batch_execute

# %% worker


@dataclass(frozen=True, kw_only=True)
class Input:
    path: str


@dataclass(frozen=True, kw_only=True)
class Output:
    urls: set[str]


@dataclass(kw_only=True)
class Worker:
    domain: str
    todo: dict
    visited: set[str]
    client: httpx.AsyncClient
    semaphore: asyncio.Semaphore

    async def handler(self, input: Input) -> Output:
        async with self.semaphore:
            resp = await self.client.get(input.path, follow_redirects=True)

        resp.raise_for_status()

        self.visited.add(input.path)

        soup = BeautifulSoup(resp, "html.parser")
        links = [
            urljoin(input.path, link["href"]) for link in soup.find_all("a", href=True)
        ]
        links = {urldefrag(l).url for l in links if self.domain == urlparse(l).hostname}

        new_links = {l for l in links if l not in self.visited}
        self.visited |= new_links
        await self.recurse(new_links)

        return Output(urls=new_links)

    async def recurse(self, links: set[str]):
        for l in links:
            arg = Input(path=l)
            i = hash(arg)
            task = asyncio.create_task(self.handler(arg), name=f"recurse_{i}")
            self.todo[task] = (str(i), arg)


# %% main


async def main():
    url = "https://emersion.fr/"
    parsed_url = urlparse(url)
    inputs = [Input(path=url)]
    tasks = {}

    async with httpx.AsyncClient(
        base_url=parsed_url._replace(path="").geturl()
    ) as client:
        worker = Worker(
            domain=parsed_url.hostname,
            todo=tasks,
            visited=set(),
            client=client,
            semaphore=asyncio.Semaphore(10),
        )
        out = await batch_execute(Worker.handler, worker, inputs, tasks=tasks)

    print(len(out))
    return out


# %% script run

if __name__ == "__main__":
    with asyncio.Runner() as runner:
        runner.run(main())
