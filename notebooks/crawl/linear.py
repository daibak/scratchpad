# %% import

from queue import Queue
from urllib import request
from urllib.parse import urldefrag, urljoin, urlparse

from bs4 import BeautifulSoup


# %%  scrape
def get_links(url: str) -> list[str]:
    resp = request.urlopen(url)
    soup = BeautifulSoup(resp.read(), "html.parser")
    return [urljoin(url, link["href"]) for link in soup.find_all("a", href=True)]


def crawl_domain(root: str) -> set[str]:
    domain = urlparse(root).hostname

    todo = Queue()
    todo.put(root)
    visited = set()

    while not todo.empty():
        url = todo.get()
        print(todo.qsize(), url)
        visited.add(url)
        news = get_links(url)
        news = {urldefrag(n).url for n in news if domain == urlparse(n).hostname}
        for n in news:
            if n not in visited:
                todo.put(n)

    return visited


def main():
    website = "https://emersion.fr/"
    out = crawl_domain(website)
    print(len(out))


if __name__ == "__main__":
    main()
