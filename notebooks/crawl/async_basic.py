# %% import

import asyncio
from urllib.parse import urldefrag, urljoin, urlparse

import httpx
from bs4 import BeautifulSoup


# %% scrape
async def get_links(url: str, sem: asyncio.Semaphore) -> list[str]:
    try:
        async with sem:
            async with httpx.AsyncClient() as client:
                resp = await client.get(url, follow_redirects=True)
    except httpx.ConnectTimeout:
        print(f"Timed out, ignoring {url}")
        return []

    soup = BeautifulSoup(resp, "html.parser")
    return [urljoin(url, link["href"]) for link in soup.find_all("a", href=True)]


async def worker(
    todo: set, visited: set, url: str, domain: str, sem: asyncio.Semaphore
):
    visited.add(url)
    news = await get_links(url, sem)
    news = {urldefrag(n).url for n in news if domain == urlparse(n).hostname}
    for n in news:
        if n not in visited:
            task = asyncio.create_task(worker(todo, visited, n, domain, sem), name=n)
            todo.add(task)


async def crawl_domain(root: str) -> set[str]:
    domain = urlparse(root).hostname

    todo = set()
    visited = set()
    sem = asyncio.Semaphore(20)

    task = asyncio.create_task(worker(todo, visited, root, domain, sem), name=root)
    todo.add(task)

    try:
        while len(todo):
            done, pending = await asyncio.wait(todo, timeout=0.5)
            todo.difference_update(done)
            print(f"todo: {len(todo)}, decrement: {len(done)}, running: {len(pending)}")
    except asyncio.CancelledError:
        for t in todo:
            t.cancel()
        done, pending = await asyncio.wait(todo, timeout=1)

    return visited


async def main():
    website = "https://emersion.fr/"
    out = await crawl_domain(website)
    print(len(out))


if __name__ == "__main__":
    with asyncio.Runner() as runner:
        runner.run(main())
