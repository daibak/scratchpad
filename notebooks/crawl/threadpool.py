# %% import

from concurrent import futures
from queue import Queue
from threading import Lock
from urllib import request
from urllib.parse import urldefrag, urljoin, urlparse

from bs4 import BeautifulSoup


# %% scrape
def get_links(url: str) -> list[str]:
    resp = request.urlopen(url)
    soup = BeautifulSoup(resp.read(), "html.parser")
    return [urljoin(url, link["href"]) for link in soup.find_all("a", href=True)]


def worker(todo, visited, url, domain):
    visited.add(url)
    news = get_links(url)
    news = {urldefrag(n).url for n in news if domain == urlparse(n).hostname}
    for n in news:
        if n not in visited:
            todo.put(n)


class LockedSet(set):
    """A set where operations are thread-safe"""

    def __init__(self, *args, **kwargs):
        self._lock = Lock()
        super().__init__(*args, **kwargs)

    def add(self, elem):
        with self._lock:
            super().add(elem)

    def remove(self, elem):
        with self._lock:
            super().remove(elem)

    def __contains__(self, elem):
        with self._lock:
            return super().__contains__(elem)

    def __len__(self):
        with self._lock:
            return super().__len__()

    def __iter__(self):
        with self._lock:
            return super().__iter__()


def crawl_domain(root: str) -> set[str]:
    domain = urlparse(root).hostname

    todo = Queue()
    visited = LockedSet()

    with futures.ThreadPoolExecutor() as executor:
        fut = executor.submit(worker, todo, visited, root, domain)

        pending = {fut}
        while pending:
            done, pending = futures.wait(
                pending, timeout=0.5, return_when=futures.ALL_COMPLETED
            )
            print(
                f"todo: {todo.qsize()}, decrement: {len(done)}, running: {len(pending)}"
            )
            while not todo.empty():
                url = todo.get()
                fut = executor.submit(worker, todo, visited, url, domain)
                pending.add(fut)

    return visited


def main():
    website = "https://emersion.fr/"
    out = crawl_domain(website)
    print(len(out))


if __name__ == "__main__":
    main()
