# %% import

import asyncio
from dataclasses import dataclass
from urllib.parse import urldefrag, urljoin, urlparse

import httpx
from bs4 import BeautifulSoup

# %% worker


@dataclass(frozen=True, kw_only=True)
class CommonWorkerParams:
    domain: str
    todo: set
    visited: set
    client: httpx.AsyncClient
    semaphore: asyncio.Semaphore


async def get_links(
    url: str, client: httpx.AsyncClient, semaphore: asyncio.Semaphore
) -> list[str]:
    try:
        async with semaphore:
            resp = await client.get(url, follow_redirects=True)
    except httpx.ConnectTimeout:
        print(f"Timed out, ignoring {url}")
        return []

    soup = BeautifulSoup(resp, "html.parser")
    return [urljoin(url, link["href"]) for link in soup.find_all("a", href=True)]


async def worker(url: str, params: CommonWorkerParams):
    params.visited.add(url)
    links = await get_links(url, params.client, params.semaphore)
    links = {urldefrag(l).url for l in links if params.domain == urlparse(l).hostname}
    for l in links:
        if l not in params.visited:
            task = asyncio.create_task(worker(l, params), name=l)
            params.todo.add(task)


# %% scrape


async def crawl_domain(root: str) -> set[str]:
    domain = urlparse(root).hostname
    todo = set()
    visited = set()
    semaphore = asyncio.Semaphore(20)

    async with httpx.AsyncClient(base_url=root) as client:
        params = CommonWorkerParams(
            domain=domain,
            todo=todo,
            visited=visited,
            client=client,
            semaphore=semaphore,
        )

        task = asyncio.create_task(worker(root, params), name=root)
        todo.add(task)

        try:
            while len(todo):
                done, pending = await asyncio.wait(todo, timeout=0.5)
                todo.difference_update(done)
                print(
                    f"todo: {len(todo)}, decrement: {len(done)}, running: {len(pending)}"
                )
        except asyncio.CancelledError:
            for t in todo:
                t.cancel()
            done, pending = await asyncio.wait(todo, timeout=1)

    return visited


async def main():
    website = "https://emersion.fr/"
    out = await crawl_domain(website)
    print(len(out))


if __name__ == "__main__":
    with asyncio.Runner() as runner:
        runner.run(main())
