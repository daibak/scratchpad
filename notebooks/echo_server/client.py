import socket
import time

with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as s:
    s.settimeout(1.0)
    s.connect(("127.0.0.1", 2000))
    for pings in range(10):
        start = time.time()
        s.sendall(b"Hello, world")
        try:
            data, server = s.recvfrom(1024)
            end = time.time()
            elapsed = end - start
            print(f"Msg:{data.decode()} - n:{pings} - elapsed:{elapsed}")
        except socket.timeout:
            print("REQUEST TIMED OUT")
