from contextlib import ExitStack, contextmanager
from typing import Callable, ContextManager


@contextmanager
def nested(*contexts):
    """
    Reimplementation of nested in python 3.
    """
    with ExitStack() as stack:
        l = []
        for ctx in contexts:
            if isinstance(ctx, Callable) and not isinstance(ctx, ContextManager):
                ctx = ctx()
            stack.enter_context(ctx)
            l.append(ctx)
        yield l
