import functools
import itertools
from concurrent import futures

try:
    from itertools import batched
except:
    # itertools.batched in py3.12
    def batched(iterable, n):
        # batched('ABCDEFG', 3) --> ABC DEF G
        if n < 1:
            raise ValueError("n must be at least one")
        it = iter(iterable)
        while batch := tuple(itertools.islice(it, n)):
            yield batch


def starmapstar(func, args):
    return list(itertools.starmap(func, args))


def flatten(nested_iterables):
    "Flatten one level of nesting"
    return itertools.chain.from_iterable(nested_iterables)


def parallel_chunked_map(fun, iterable, chunksize=1):
    todo = {}
    out = {}

    fun = functools.partial(starmapstar, fun)
    with futures.ProcessPoolExecutor() as executor:
        for batch in batched(iterable, chunksize):
            fut = executor.submit(fun, batch)
            todo[fut] = batch
            if len(todo) < executor._max_workers:
                continue
            done, pending = futures.wait(
                todo.keys(), timeout=0, return_when=futures.FIRST_COMPLETED
            )
            print(
                f"todo: {len(todo)}, just_done: {len(done)}, pending: {len(pending)}",
                end="\r",
                flush=True,
            )
            for fut in done:
                keys = todo.pop(fut)
                for key, value in zip(keys, fut.result()):
                    out[key] = value
        print("\nfull queue", flush=True)
        while todo:
            done, pending = futures.wait(
                todo.keys(), timeout=1, return_when=futures.ALL_COMPLETED
            )
            print(
                f"todo: {len(todo)}, just_done: {len(done)}, pending: {len(pending)}",
                end="\r",
                flush=True,
            )
            for fut in done:
                keys = todo.pop(fut)
                for key, value in zip(keys, fut.result()):
                    out[key] = value
        print()

    return out


def parallel_map(fun, iterable):
    todo = {}
    out = {}

    with futures.ProcessPoolExecutor() as executor:
        for args in iterable:
            fut = executor.submit(fun, args)
            todo[fut] = args
            if len(todo) < executor._max_workers:
                continue
            done, pending = futures.wait(
                todo.keys(), timeout=0, return_when=futures.FIRST_COMPLETED
            )
            print(
                f"todo: {len(todo)}, just_done: {len(done)}, pending: {len(pending)}",
                end="\r",
                flush=True,
            )
            for fut in done:
                key = todo.pop(fut)
                out[key] = fut.result()
        print("\nfull queue", flush=True)
        while todo:
            done, pending = futures.wait(
                todo.keys(), timeout=1, return_when=futures.ALL_COMPLETED
            )
            print(
                f"todo: {len(todo)}, just_done: {len(done)}, pending: {len(pending)}",
                end="\r",
                flush=True,
            )
            for fut in done:
                key = todo.pop(fut)
                out[key] = fut.result()
        print()

    return out
