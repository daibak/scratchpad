import asyncio
from collections.abc import Iterable, Sized
from typing import Callable

from tqdm import tqdm


async def batch_execute[I, P, O](
    worker: Callable[[P, I], O],
    params: P,
    inputs: Iterable[I],
    update_period: float = 0.5,
    disable_tqdm=None,
    tasks: dict | None = None,
    total: int | None = None,
) -> dict[int, tuple[I, O]]:
    if tasks is None:
        tasks = {}
        if total is None and isinstance(inputs, Sized):
            total = len(inputs)

    for i, x in enumerate(inputs):
        task = asyncio.create_task(worker(params, x), name=f"batch_exec_{i}")
        tasks[task] = (i, x)

    output = {}
    try:
        with tqdm(total=total, disable=disable_tqdm) as pbar:
            while len(tasks):
                done, _ = await asyncio.wait(tasks, timeout=update_period)
                for task in done:
                    i, x = tasks.pop(task)
                    output[i] = (x, task.result())
                pbar.update(len(done))

    except asyncio.CancelledError:
        for task in tasks:
            task.cancel()
        done, _ = await asyncio.wait(tasks, timeout=1)

    return output
