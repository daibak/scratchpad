from functools import wraps


def decorator(*deco_args):
    """Example decorator with arguments"""

    def inner(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            # before
            print(deco_args)
            out = f(*args, **kwargs)
            # after
            return out

        return wrapped

    return inner


# Memoization

from collections import OrderedDict


def decorator(limit: int = 10):
    def inner(f):
        cache = OrderedDict()

        @wraps(f)
        def wrapped(*args, **kwargs):
            key = (args, kwargs)
            cached = cache.get(key, None)
            if cached is not None:
                return cached

            out = f(*args, **kwargs)

            if len(out) >= limit:
                out.popitem(last=False)
            cache[key] = out

            return out

        return wrapped

    return inner
